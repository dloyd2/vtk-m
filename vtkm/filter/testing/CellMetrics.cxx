//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 National Technology & Engineering Solutions of Sandia, LLC (NTESS).
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-NA0003525 with NTESS,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#include <vtkm/cont/ArrayRangeCompute.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include "vtkm/cont/DynamicArrayHandle.h"
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/filter/CellMeasures.h>
#include <vector>
/*
   CellMetrics.cxx takes an input vtk file and generates a new field on that dataSet. This data set is then output to a given filename. 
   If no filename is given, then "output.vtk" is used. The mesh quality of the new field is determined and output to std::cout
*/
using namespace vtkm::cont;

ArrayHandle<vtkm::FloatDefault> *
TestMetric(vtkm::io::writer::VTKDataSetWriter *writer, DataSet data)
{
  //
  //   Takes a pointer to a DataSetWriter and a set of data as parameters. Executes a filter on the data set to generate a new field.
  //   Writes the resulting DataSet into an output file and returns the new field for calculation. NOTE: If a field with the same name as the 
  //   new field is already in the DataSet, TestMetric will return NULL
  //
  vtkm::filter::CellMeasures<vtkm::AllMeasures> c_measure;
  c_measure.SetCellMeasureName("volume");
  ArrayHandle<vtkm::FloatDefault> *metricArrPtr = new ArrayHandle<vtkm::FloatDefault>;
  if (!data.HasField(c_measure.GetCellMeasureName()))
  {
    DataSet outputData =  c_measure.Execute(data);  //TODO: try catch this for vtkm::cont::ErrorExecution
    DynamicArrayHandle temp = outputData.GetField(c_measure.GetCellMeasureName()).GetData();
    DataSetFieldAdd::AddCellField(outputData, c_measure.GetCellMeasureName(), temp, "cells");
    writer->WriteDataSet(outputData);
    temp.CopyTo(*metricArrPtr);
  }
  else 
  {
    std::cerr << "data field already in DataSet" << std::endl;
    delete metricArrPtr;
    metricArrPtr = NULL;
  }
  return metricArrPtr;
}

void
DetermineMeshQuality(ArrayHandle<vtkm::FloatDefault> *metricArrayPtr)
{
  //
  //   Takes a pointer to an ArrayHandle as a parameter. Determine mesh quality of the new field stored in ArrayHandle
  //
  auto metricPortal = metricArrayPtr->GetPortalConstControl();
  ArrayHandle<vtkm::Range> rangeArray = ArrayRangeCompute(*metricArrayPtr);
  auto rangePortal = rangeArray.GetPortalConstControl();
  vtkm::Range fieldRange = rangePortal.Get(0);

  vtkm::Float32 numCells = metricArrayPtr->GetNumberOfValues();
  vtkm::Float64 min = fieldRange.Min;
  vtkm::Float64 max = fieldRange.Max;
  
  vtkm::Id valueToAdd;
  vtkm::Float32 sum          = 0;
  vtkm::Float32 mean         = 0;
  vtkm::Float32 variance     = 0;
  vtkm::Float32 squaredTotal = 0;
  vtkm::Float32 valueToTest  = 0;
  vtkm::Float32 numToSquare  = 0;
  for (valueToAdd = 0; valueToAdd < numCells; valueToAdd++)
   {
    valueToTest = metricPortal.Get(valueToAdd);
    sum += valueToTest;
   }
  mean = sum / numCells;
  for (valueToAdd = 0; valueToAdd < numCells; valueToAdd++)
   {
    valueToTest = metricPortal.Get(valueToAdd);
    numToSquare = valueToTest - mean;
    squaredTotal += (numToSquare * numToSquare);
   }
  variance = squaredTotal / numCells;

  std::cout << "Mesh quality of generated field:\n"
	    << "----------------------------------\n" 
	    << "Number of cells: " << numCells << "\n"
	    << "Mean:            " << mean << "\n" 
	    << "Variance:        " << variance << "\n"
	    << "Minimum:         " << min << "\n"
	    << "Maximum:         " << max << std::endl;
}

int main(int argc, char* argv[])
{
  //
  //  Check usage, set writer and read input
  //
  vtkm::io::writer::VTKDataSetWriter *writerPtr = NULL;
  switch (argc)
  {
    case 1:
      std::cerr << "Usage: " << std::endl
                << "$ " << argv[0] << " <input.vtk_file> <output.vtk_fileName>"
                << std::endl;
      return 1;

    case 2:
      {
      vtkm::io::writer::VTKDataSetWriter defaultWriter("output.vtk");
      writerPtr = &defaultWriter;
      }
      break;

    default:
      vtkm::io::writer::VTKDataSetWriter dynamicWriter(argv[argc - 1]);
      writerPtr = &dynamicWriter;
      break;
  }
  DataSet input;
  ArrayHandle<vtkm::FloatDefault> *metricArrayPtr;
  vtkm::io::reader::VTKDataSetReader reader(argv[1]);
  try
  {
    input = reader.ReadDataSet();  //FIELD problem here
  }
  catch (vtkm::io::ErrorIO&)
  {
    std::cerr << "Error occured while reading input. Exiting" << std::endl;
    return 1;
  }
  try
  {
    metricArrayPtr = TestMetric(writerPtr, input);
  }
  catch (vtkm::io::ErrorIO&)
  {
    std::cerr << "Error occured while writing output. Exiting" << std::endl;
    return 1;
  }
  if (metricArrayPtr == NULL)
  {
     return 1;
  }
  DetermineMeshQuality(metricArrayPtr);
  delete metricArrayPtr;
  return 0;
}
